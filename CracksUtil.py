# import the necessary packages

import numpy as np
import cv2 as cv
import os

print("SCRIPT INIT")

DEBUG_MODE = True

def removeBackgorund(image, mask):
    return cv.bitwise_and(image, image, mask=mask)

def displayImage(text, image):
    cv.imshow(text, image)
    cv.waitKey(0)

def debugImage(text, image):
    if DEBUG_MODE:
        cv.imshow(text, image)
        cv.waitKey(0)

def getObjectAreaSize(img_edges):
    contours, hierarchy = cv.findContours(img_edges, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    maxSize = 0
    for con in contours:
        approxCurve = cv.approxPolyDP(con, 10, True)
        area = cv.contourArea(approxCurve)
        if area > maxSize:
            maxSize = area

    print("Object area: ", maxSize)
    return maxSize

def getAverageObjectColorHSV(image, mask):
    b, g, r, t = cv.mean(image, mask)
    bgrAverage = np.uint8([[[b, g, r]]])
    meanHSV = cv.cvtColor(bgrAverage, cv.COLOR_BGR2HSV)
    h, s, v = meanHSV[0][0]
    return (h, s, v)

def hisEqulColor(img):
    ycrcb = cv.cvtColor(img, cv.COLOR_BGR2YCR_CB)
    channels = cv.split(ycrcb)
    len(channels)
    cv.equalizeHist(channels[0], channels[0])
    cv.merge(channels, ycrcb)
    cv.cvtColor(ycrcb, cv.COLOR_YCR_CB2BGR, img)
    return img