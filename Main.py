import fruit_detection_m as fd
import fruit_data_transfer
import re
import os
import mould_detection as md
import cv2 as cv
import CracksDetector as cd
import geometry_defect as gd
import numpy as np
import matplotlib.pyplot as plt

dir = 'DataSet/'
res = [] # tablica wynikowa zawierajaca napisy jaki owoc
i = 0
dir_child = [x[0] for x in os.walk(dir)]
#dir_child = [dir, dir + 'rotten_banana']
#print(dir_child)

overall_good = 0
overall_all = 0
for dir_sub in dir_child[2:]:
    print(dir_sub)
    res = []
    i = 0
    if re.search('banana', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.BANANA
    elif re.search('strawberries', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.STRAWBERRY
    elif re.search('lemon', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.LEMON
    elif re.search('orange', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.ORANGE
    else:
        pass

    for filename in os.listdir(dir_sub):
        print('Checking image: ', (dir_sub + '/' + filename))
        fr = fd.fruit_detection(dir_sub + '/' + filename, key_fruit)
        wynik = fr.detect_fruit()

        img_original=cv.imread(dir_sub + '/' + filename)
        img_out = np.zeros((img_original.shape[0], img_original.shape[1]))
        title = ''

        if wynik.fruit_type == fruit_data_transfer.Fruit.BANANA:
            flag1, img_out = cd.cracks_detection(wynik.img_original, wynik.img_mask, cd.Fruit.BANANA, False)
            if flag1==False:
                flag2, img_out = md.mould_detection(wynik.img_original, wynik.img_mask, md.Fruit.BANANA)
                if flag2 == False:
                    title = 'Banana + Clean'
                else:
                    title = 'Banana + mould'
            else:
                title = 'Banana + crack'

        if wynik.fruit_type == fruit_data_transfer.Fruit.ORANGE:
            flag3, img_out = cd.cracks_detection(wynik.img_original, wynik.img_mask, cd.Fruit.ORANGE, False)
            if flag3==False:
                flag4, img_out = md.mould_detection(wynik.img_original, wynik.img_mask, md.Fruit.ORANGE)
                if flag4 == False:
                    flag5, img_out = gd.find_geometry_defect(wynik.img_original, wynik.img_mask, gd.Fruit.ORANGE)
                    if flag5 == False:
                        title = 'Orange + Clean'
                    else:
                        title = 'Orange + geometry defect'
                else:
                    title = 'Orange + mould'
            else:
                title = 'Orange + crack'

        if wynik.fruit_type == fruit_data_transfer.Fruit.STRAWBERRY:
            [flag6, img_out] = md.mould_detection(wynik.img_original, wynik.img_mask, md.Fruit.STRAWBERRY)
            if flag6==False:
                flag7, img_out = gd.find_geometry_defect(wynik.img_original, wynik.img_mask, gd.Fruit.STRAWBERRY)
                if flag7 == False:
                    title = 'Strawberry + Clean'
                else:
                    title = 'Strawberry + geometry defect'
            else:
                title = 'Strawberry + mould'

        #img_original = cv.cvtColor(img_original, cv.COLOR_BGR2RGB)
        #img_out = cv.cvtColor(img_out, cv.COLOR_BGR2RGB)
        img_mask = cv.cvtColor(wynik.img_mask, cv.COLOR_GRAY2RGB)

        two_pictures1 = np.hstack((wynik.img_original, img_mask, img_out))
        plt.imshow(two_pictures1)
        plt.title(title)
        plt.show()
        #zapisywanie
        #path='results' +'/'+ dir_sub + '/' + filename
        #cv.imwrite(path, two_pictures1)
        #wyświelanie
        #cv.imshow(title, two_pictures1)