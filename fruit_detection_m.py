import cv2
import numpy as np
import argparse
import copy
from math import *
import os
import matplotlib.pyplot as plt
import statistics
import re
import fruit_data_transfer

class fruit_detection:
    def __init__(self, filename, fruit_name = ''):
        # w tym miejscu wykonujemy elementarne operacje, ktĂłre majÄ… podnieĹ›Ä‡ wyrazistoĹ›Ä‡ obrazu i uĹ‚atwiÄ‡ dalsze przetwarzanie
        self.filename = filename
        self.img_original = cv2.imread(self.filename, 1)
        self.height, self.width, depth = self.img_original.shape

        # scale down an image for faster computation
        scale = self.height / 250

        self.height, self.width = int(self.height/scale), int(self.width/scale)

        self.img_original = cv2.resize(self.img_original,(self.width, self.height))
        self.img_resized = copy.copy(self.img_original)
        
        self.fruits = {'orange' : 0,
                       'banana' : 1,
                       'strawberry' : 2
                      }
        # to be filled later
        self.img_gray_masked = {'orange' : 0,
                                'banana' : 1,
                                'strawberry' : 2
                                }
        self.img_hsv_masked = {'orange' : 0,
                               'banana' : 1,
                               'strawberry' : 2
                                }

        self.img_original = self.hisEqulColor(self.img_original)

        img_hsv = cv2.cvtColor(self.img_resized.copy(), cv2.COLOR_BGR2HSV)

        #znajdujemy maski dla kolorĂłw owocĂłw, ktĂłre przetwarzamy
        self.mask_orange = cv2.inRange(img_hsv, (5, 170, 20), (20, 255, 255))
        self.mask_yellow = cv2.inRange(img_hsv, (20, 100, 20), (40, 255, 255))
        self.mask_red = cv2.inRange(img_hsv, (0, 100, 20), (5, 255, 255)) + cv2.inRange(img_hsv, (160, 100, 20),
                                                                                    (180, 255, 255))



        self.contour = []

        self.fruit_name = fruit_name

        self.fruit_data = fruit_data_transfer.fruit_data_transfer(filename)
        self.fruit_data.img_original = cv2.cvtColor(self.img_resized.copy(), cv2.COLOR_BGR2RGB)

    def hisEqulColor(self, img):
        ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)
        channels = cv2.split(ycrcb)
        print
        len(channels)
        cv2.equalizeHist(channels[0], channels[0])
        cv2.merge(channels, ycrcb)
        cv2.cvtColor(ycrcb, cv2.COLOR_YCR_CB2BGR, img)
        return img


    def show(self):
       # cv2.imshow('image', self.img_original)
       # cv2.waitKey(0)
        plt.imshow(self.img_original)
        plt.show()

    def show(self, file):
        plt.imshow(file, 'gray')
        plt.show()
    # show pyplot with title
    def show(self, file, title):
        plt.imshow(file, 'gray')
        plt.title(title)
        plt.show()

    ## Basic masks
    ## @param mask_color defines type of mask
    # if mask_color is orange or banana, then get_mask_hsv returns image masked with wide orange (from orange to bright yellow)
    # if mask_color is strawberry, then get_mask_hsv returns red mask
    # else mask_color returns white to gray mask
    def get_mask_hsv(self, mask_color):

        img_original = self.img_original.copy()
        img_hsv = cv2.cvtColor(img_original, cv2.COLOR_BGR2HSV)

        mask_v2 = cv2.inRange(img_hsv, (0,0,0), (180, 25, 255))
        mask_white_neg = cv2.inRange(img_hsv, (0, 0, 0), (180, 255, 45))      
        mask_white_neg = cv2.bitwise_or(mask_white_neg, mask_v2)    

        mask_white_neg = cv2.bitwise_not(mask_white_neg)

        mask_wide_orange = cv2.inRange(img_hsv, (5, 0, 0), (50, 255, 255))

        mask_strawberry = cv2.inRange(img_hsv, (0, 100, 20), (5, 255, 255))

        if (mask_color == 'orange' or  mask_color == 'banana'):
            mask = mask_wide_orange
        elif (mask_color == 'strawberry'):
            mask = mask_strawberry
        else:
            mask = mask_white_neg

        return mask

    
    ## window filter, divides image into window, check if mean saturation is greater than threshold. If not, enlarge window and check again. 
    # It is designed to decide, whether area below threshold is inside a fruit. If it is, it should reach pixels with greater saturation, thus keep areas inside a fruit
    def get_img_masked2(self, img):

        img_hsv = img 
        
        img_hsv = np.array(img_hsv)
        img_hsv_copy = img_hsv.copy()


        step = 15 # size of window side
        base_threshold = 15
        h = self.height - 1
        w = self.width - 1
        threshold =  (int(img_hsv[0,0,1]) + int(img_hsv[h,0,1]) + int(img_hsv[0,w,1]) + int(img_hsv[h,w,1]))//4 # mean saturation of background calculated from image vertices 
        thresh_amp = 1.5 # threshold amplifier
        mask = [0,0,0]

        square_enlarge = 50 # how much will window enlarge, reaches side of size: step + 2*square_enlarge

       
        # There are 4 loops, each starts from different vertex
        for x in range(0, w - (w)%step, step):
            for y in range(0, h - (h)%step, step):

                if np.sum(img_hsv[y:y+step,x:x+step,1]) < (threshold + base_threshold)*step**2 :#and np.sum(img_hsv[y:y+step,x,2]) > thresh_amp*threshold and np.sum(img_hsv[y:y+step,x,2]) < 200:
                    in_fruit = False
                    for i in range(1, square_enlarge, 1):
                        if np.sum(img_hsv[y-i:y+step+i,x-i:x+step+i,1]) > (threshold + base_threshold)*(step + i)**2:
                            in_fruit = True
                            break
                    if not in_fruit:
                        #if not step*35 < np.sum(img_hsv[y:y+step,x:x+step,0]) < threshold < step*130 :
                        swap_color = np.array([mask]*step**2).reshape(step,step,3)
                        img_hsv[y:y+step,x:x+step,:] = swap_color
        
        for x in range(w - step, 0 + (w)%step, -step):
            for y in range(0, h - (h)%step, step):

                if np.sum(img_hsv[y:y+step,x:x+step,1]) < (threshold + base_threshold)*step**2 :#and np.sum(img_hsv[y:y+step,x,2]) > thresh_amp*threshold and np.sum(img_hsv[y:y+step,x,2]) < 200:
                    in_fruit = False
                    for i in range(1, square_enlarge, 1):
                        if np.sum(img_hsv[y-i:y+step+i,x-i:x+step+i,1]) > (threshold + base_threshold)*(step + i)**2:
                            in_fruit = True
                            break
                    if not in_fruit:
                        #if not step*35 < np.sum(img_hsv[y:y+step,x:x+step,0]) < threshold < step*130 :
                        swap_color = np.array([mask]*step**2).reshape(step,step,3)
                        img_hsv[y:y+step,x:x+step,:] = swap_color


        for x in range(0, w - (w)%step, step):
            for y in range( h - step, 0 + (h)%step,  -step):

                if np.sum(img_hsv[y:y+step,x:x+step,1]) < (threshold + base_threshold)*step**2 :#and np.sum(img_hsv[y:y+step,x,2]) > thresh_amp*threshold and np.sum(img_hsv[y:y+step,x,2]) < 200:
                    in_fruit = False
                    for i in range(1, square_enlarge, 1):
                        if np.sum(img_hsv[y-i:y+step+i,x-i:x+step+i,1]) > (threshold + base_threshold)*(step + i)**2:
                            in_fruit = True
                            break
                    if not in_fruit:
                        #if not step*35 < np.sum(img_hsv[y:y+step,x:x+step,0]) < threshold < step*130 :
                        swap_color = np.array([mask]*step**2).reshape(step,step,3)
                        img_hsv[y:y+step,x:x+step,:] = swap_color


        
        for x in range(w - step, 0 + (w)%step, -step):
            for y in range( h - step, 0 + (h)%step, -step):

                if np.sum(img_hsv[y:y+step,x:x+step,1]) < (threshold + base_threshold)*step**2 :#and np.sum(img_hsv[y:y+step,x,2]) > thresh_amp*threshold and np.sum(img_hsv[y:y+step,x,2]) < 200:
                    in_fruit = False
                    for i in range(1, square_enlarge, 1):
                        if np.sum(img_hsv[y-i:y+step+i,x-i:x+step+i,1]) > (threshold + base_threshold)*(step + i)**2:
                            in_fruit = True
                            break
                    if not in_fruit:
                        #if not step*35 < np.sum(img_hsv[y:y+step,x:x+step,0]) < threshold < step*130 :
                        swap_color = np.array([mask]*step**2).reshape(step,step,3)
                        img_hsv[y:y+step,x:x+step,:] = swap_color
        

        return(img_hsv)

    ## Fourier high pass filter
    def get_img_masked3(self, img):
        #img = self.img_resized.copy()
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = np.array(img)

        dft = cv2.dft(np.float32(img), flags=cv2.DFT_COMPLEX_OUTPUT)
        dft_shift = np.fft.fftshift(dft)

        magnitude_spectrum = 20 * np.log(cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))
        rows, cols = img.shape
        crow, ccol = int(rows / 2), int(cols / 2)  # center

        mask = np.ones((rows, cols, 2), np.uint8)
        r = 80
        center = [crow, ccol]
        x, y = np.ogrid[:rows, :cols]
        mask_area = (x - center[0]) ** 2 + (y - center[1]) ** 2 <= r*r
        mask[mask_area] = 1

        # apply mask and inverse DFT
        fshift = dft_shift * mask

        fshift_mask_mag = 2000 * np.log(cv2.magnitude(fshift[:, :, 0], fshift[:, :, 1]))

        f_ishift = np.fft.ifftshift(fshift)
        img_back = cv2.idft(f_ishift)
        img_back = cv2.magnitude(img_back[:, :, 0], img_back[:, :, 1])

        

        thresh = (int(img_back[0,0]) + int(img_back[rows-1, 0]) + int(img_back[0, cols -1]) + int(img_back[rows-1, cols-1]) )//4 # Threshold calculated from mean value of vertices pixels
        thresh_adjust = 0.99

        img_back = np.expand_dims(img_back, axis = 2)

        img_hsv = self.img_resized.copy()
        img_hsv = cv2.cvtColor(img_hsv, cv2.COLOR_BGR2HSV)

        mask = np.array([0,0,0]*rows*cols).reshape(rows, cols, 3)

        img_masked = np.where(img_back < thresh_adjust*thresh, img_hsv, mask)

        return img_masked


    ## Vector filter, divides image into vectors, if mean saturation lesser than threshold, then it's background. Eagerly classifies defects as background
    def get_img_masked4(self, img):
        #img = self.img_resized.copy()
        img_hsv = img # cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        

        img_hsv = np.array(img_hsv)
        img_hsv_copy = img_hsv.copy()

        step = 20
        base_threshold = 20
        h = self.height - 1
        w = self.width - 1
        edge_size = 3
        threshold =  np.sum((img_hsv[0:edge_size,0:edge_size,1] + img_hsv[h - edge_size:h,0:edge_size,1] + img_hsv[0:edge_size,w - edge_size: w,1] \
             + img_hsv[h - edge_size:h,w - edge_size:w,1])/(4*edge_size**2)) + base_threshold*step
        thresh_amp = 1.5

        mask = [0, 0, 0]



        for x in range(self.width - 1):
            for y in range(0, self.height - 1 - (self.height - 1)%step, step):

                if np.sum(img_hsv[y:y+step,x,1]) < threshold :#and np.sum(img_hsv[y:y+step,x,2]) > thresh_amp*threshold and np.sum(img_hsv[y:y+step,x,2]) < 200:
                    swap_color = np.array([mask]*step)
                    img_hsv[y:y+step,x,:] = swap_color
        
        for x in range(0, self.width - 1 - (self.width -1)%step, step):
            for y in range(self.height -1):

                if np.sum(img_hsv[y,x:x+step,1]) < threshold :#and np.sum(img_hsv[y:y+step,x,2]) > thresh_amp*threshold and np.sum(img_hsv[y:y+step,x,2]) < 200:
                    swap_color = np.array([mask]*step)
                    img_hsv[y,x:x+step,:] = swap_color

        return img_hsv

    def waterShade(self,image):
        img = image

        # podbicie wibrancji
        image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        image[:, :, 1] = (image[:, :, 1] * 2.8).clip(0, 255)
        image = cv2.cvtColor(image, cv2.COLOR_HSV2RGB)

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 9, 3)
        #plt.imshow(thresh,'gray')
        #plt.show()

        kernel = np.ones((3, 3), np.uint8)
        thresh = cv2.dilate(thresh, kernel, iterations=1)

        #wypeĹ‚nienie dziur w masce
        im_floodfill = thresh.copy()
        h, w = thresh.shape[:2]
        mask2 = np.zeros((h + 2, w + 2), np.uint8)
        cv2.floodFill(im_floodfill, mask2, (0, 0), 255);
        im_floodfill_inv = cv2.bitwise_not(im_floodfill)
        thresh = thresh | im_floodfill_inv
        #plt.imshow(thresh,'gray')
        #plt.show()

        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=5)
        #plt.imshow(opening,'gray')
        #plt.show()

        #sure background
        sure_bg = cv2.dilate(thresh, kernel, iterations=3)
        #plt.imshow(sure_bg,'gray')
        #plt.show()

        dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
        #plt.imshow(dist_transform)
        #plt.show()

        #sure foreground
        ret, sure_fg = cv2.threshold(dist_transform, 0.2 * dist_transform.max(), 255, 0)
        sure_fg = np.uint8(sure_fg)
        #plt.imshow(sure_fg,'gray')
        #plt.show()

        unknown = cv2.subtract(sure_bg, sure_fg)
        ret, markers = cv2.connectedComponents(sure_fg)
        markers = markers + 1
        markers[unknown == 255] = 0
        markers = cv2.watershed(image, markers)
        img[markers == -1] = [255, 0, 0]

        markers = cv2.convertScaleAbs(markers)
        ret, thresh = cv2.threshold(markers,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        res = cv2.bitwise_and(img,img,mask = thresh)

        #plt.imshow(res)
        #plt.show()

        return thresh


    def water_shed_stand_alone(self,image):
        img_original = image.copy()
        img = image

        # podbicie wibrancji
        image = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
        image[:, :, 1] = (image[:, :, 1] * 2.8).clip(0, 255)
        image = cv2.cvtColor(image, cv2.COLOR_HSV2RGB)

        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 9, 3)
        #plt.imshow(thresh,'gray')
        #plt.show()

        kernel = np.ones((3, 3), np.uint8)
        thresh = cv2.dilate(thresh, kernel, iterations=1)

        #wypeĹ‚nienie dziur w masce
        im_floodfill = thresh.copy()
        h, w = thresh.shape[:2]
        mask2 = np.zeros((h + 2, w + 2), np.uint8)
        cv2.floodFill(im_floodfill, mask2, (0, 0), 255)
        im_floodfill_inv = cv2.bitwise_not(im_floodfill)
        thresh = thresh | im_floodfill_inv
        #plt.imshow(thresh,'gray')
        #plt.show()

        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=5)
        #plt.imshow(opening,'gray')
        #plt.show()

        #sure background
        sure_bg = cv2.dilate(thresh, kernel, iterations=3)
        #plt.imshow(sure_bg,'gray')
        #plt.show()

        dist_transform = cv2.distanceTransform(opening, cv2.DIST_L2, 5)
        #plt.imshow(dist_transform)
        #plt.show()

        #sure foreground
        ret, sure_fg = cv2.threshold(dist_transform, 0.2 * dist_transform.max(), 255, 0)
        sure_fg = np.uint8(sure_fg)
        #plt.imshow(sure_fg,'gray')
        #plt.show()

        unknown = cv2.subtract(sure_bg, sure_fg)
        ret, markers = cv2.connectedComponents(sure_fg)
        markers = markers + 1
        markers[unknown == 255] = 0
        markers = cv2.watershed(image, markers)
        img[markers == -1] = [255, 0, 0]

        markers = cv2.convertScaleAbs(markers)
        ret, thresh = cv2.threshold(markers,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        res = cv2.bitwise_and(img,img,mask = thresh)



        thresh = thresh > 0
        truth_table = np.expand_dims(thresh, axis = 2)
        t = np.concatenate((truth_table,truth_table, truth_table), axis = 2)
        cols,rows,_ = img_original.shape
        mask = np.array([255,0,255]*rows*cols).reshape(cols, rows, 3)
        img_out = np.where(t, img_original, mask).astype(np.uint8)

        if False:
            plt.imshow(img_out)
            plt.show()

        white = np.array([255,255,255]*rows*cols).reshape(cols, rows, 3).astype(np.uint8)
        black = np.array([0,0,0]*rows*cols).reshape(cols, rows, 3).astype(np.uint8)

        self.fruit_data.img_mask = np.where(t, white, black).astype(np.uint8)
        self.fruit_data.img_mask = self.fruit_data.img_mask[:,:,0]

        return thresh


    ## Combines masking methods, if pixel is on at least 2 images, then pixel is kept, else it's classified as background
    # @param show if True, masks, original picture and final masked picture is shown
    def mask_background1(self, show=False):
        img_hsv = cv2.cvtColor(self.img_resized.copy(), cv2.COLOR_BGR2HSV)

        
        im_original = self.img_resized.copy()
        #im1 = self.get_img_masked2(img_hsv.copy()).astype(np.uint8)
        im2 = self.get_img_masked3(self.img_resized.copy()).astype(np.uint8)
        im3 = self.get_img_masked4(img_hsv.copy()).astype(np.uint8)
        

        #im1 = cv2.cvtColor(im1, cv2.COLOR_HSV2BGR)
        im2 = cv2.cvtColor(im2, cv2.COLOR_HSV2BGR)
        im3 = cv2.cvtColor(im3, cv2.COLOR_HSV2BGR)

        #im1_show = im1.copy()
        im2_show = im2.copy()
        im3_show = im3.copy()

        #im1_show = cv2.cvtColor(im1_show, cv2.COLOR_BGR2RGB)
        im2_show = cv2.cvtColor(im2_show, cv2.COLOR_BGR2RGB)
        im3_show = cv2.cvtColor(im3_show, cv2.COLOR_BGR2RGB)


        #im1 = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
        im2 = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)
        im3 = cv2.cvtColor(im3, cv2.COLOR_BGR2GRAY)

        # added watershed method
        im1 = self.waterShade(self.img_resized.copy())
        im1_show = im1.copy()

        im1 = im1 > 0
        im2 = im2 > 0
        im3 = im3 > 0

        rows, cols = self.width, self.height


        truth_table = np.logical_or(np.logical_and(im2,im3),np.logical_and(im1,np.logical_or(im2,im3)))

        truth_table = np.expand_dims(truth_table, axis = 2)

        t = np.concatenate((truth_table,truth_table, truth_table), axis = 2)

        #im_original = np.array(im_original).astype(np.uint8)
        

        #print(t.shape)

        mask = np.array([255,0,255]*rows*cols).reshape(cols, rows, 3)

        white = np.array([255,255,255]*rows*cols).reshape(cols, rows, 3).astype(np.uint8)
        black = np.array([0,0,0]*rows*cols).reshape(cols, rows, 3).astype(np.uint8)

        self.fruit_data.img_mask = np.where(t, white, black).astype(np.uint8)
        self.fruit_data.img_mask = self.fruit_data.img_mask[:,:,0]

        fin = np.where(t, im_original, mask).astype(np.uint8)
        
        #fin_col_con = cv2.cvtColor(fin.copy(), cv2.COLOR_HSV2BGR)
        fin_col_con = cv2.cvtColor(fin.copy(), cv2.COLOR_BGR2RGB)

        fin_gray = cv2.cvtColor(fin.copy(), cv2.COLOR_HSV2BGR)
        fin_gray = cv2.cvtColor(fin_gray, cv2.COLOR_BGR2GRAY)

        contours, _ = cv2.findContours(fin_gray, cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_NONE)

        contour = max(contours, key = cv2.contourArea)
       

        cv2.drawContours(fin_col_con, contour, -1, [0,0,0], 3)   

        if show:
            size = 5

            fig, _ = plt.subplots(1, size, constrained_layout = True)

            plt.subplot(1, size, 1), plt.imshow(im2_show)
            plt.title('Fourier'), plt.xticks([]), plt.yticks([])

            plt.subplot(1, size, 2), plt.imshow(im1_show)
            plt.title('Watershed'), plt.xticks([]), plt.yticks([])

            plt.subplot(1, size, 3), plt.imshow(im3_show)
            plt.title('Stripes'), plt.xticks([]), plt.yticks([])

            plt.subplot(1, size, 4), plt.imshow(cv2.cvtColor(self.img_resized.copy(), cv2.COLOR_BGR2RGB))
            plt.title('Original'), plt.xticks([]), plt.yticks([])

            plt.subplot(1, size, 5), plt.imshow(fin_col_con)
            plt.title('Masked'), plt.xticks([]), plt.yticks([])

            

            plt.get_current_fig_manager().window.state('zoomed')

            fig.suptitle(self.filename, fontsize = 16)
            fig.canvas.set_window_title(self.filename)
            

            plt.show() 
        
        return fin_col_con

             
    def find_contours(self, fruit):
        
        img_original = self.img_original
        img_hsv = cv2.cvtColor(img_original, cv2.COLOR_BGR2HSV)

        mask = self.get_mask_hsv(fruit)
        target = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)

        img_gray = cv2.cvtColor(target, cv2.COLOR_BGR2GRAY)

        if fruit in self.fruits.keys():
            self.img_gray_masked[fruit] = img_gray
            self.img_hsv_masked[fruit] = img_hsv

        contours, _ = cv2.findContours(img_gray.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_NONE)

        
        if fruit not in self.fruits.keys() and False:
            cnt = max(contours, key = cv2.contourArea)
            cv2.drawContours(img_gray, cnt, -1, (255,255,255), 1)
            cv2.imshow(self.filename, img_gray)
            cv2.waitKey(0)

        return contours, img_gray

    def detect_circle(self, contour):

        (x,y), r = cv2.minEnclosingCircle(contour)

        return(int(x), int(y)), int(r)
    
    def fruit_decision(self, orange, yellow, red, circle_ratio):
        # w zaleĹĽnoĹ›ci od tego, ktĂłrego koloru jest najwiÄ™cej, podejmujemy decyzjÄ™ o typie owocu
        maxim = max(orange, yellow, red)
        if maxim == orange:
            return 'orange'
        if maxim == red:
            return 'strawberry'
        if maxim == yellow: # w przypadku ĹĽĂłĹ‚tego badamy jak bardzo ksztaĹ‚t przypomina koĹ‚o. jeĹ›li sĹ‚abo to jest to banan, jeĹ›li mocno, to to cytryna
            if circle_ratio < 0.8:
                return 'banana'
            else:
                return 'lemon'

    def distance_points(self, point1, point2):
        return pow((point1[0] - point2[0])**2 + (point1[1] - point2[1])**2, 0.5)
    
    ## Find 2 furthest points of contour
    # @param contour
    def extreme_points(self, contour):
        contour = np.array(contour)
        #print(contour.shape)
        point_id = []
        point_id.append(contour[contour[:,:,0].argmin()])
        point_id.append(contour[contour[:,:,0].argmax()])
        point_id.append(contour[contour[:,:,1].argmin()])
        point_id.append(contour[contour[:,:,1].argmax()])

        point1, point2, distance = 0, 0, 0

        for i in range(4):
            for j in range(i + 1, 4):
                temp = self.distance_points(point_id[i][0][:], point_id[j][0][:])
                if temp > distance:
                    point1 = point_id[i][0][:]
                    point2 = point_id[j][0][:]
                    distance = temp
        return point1, point2, distance


    def detect_fruit(self):
        # shape detection section
        cnt, _ = self.find_contours('background') # znajdujemy kontur oraz maske owocu
        # ponizej badamy jak bardzo znaleziony kontur jest podobny do koĹ‚a (moĹĽe siÄ™ przydaÄ‡ przy okreslaniu typu owoca)
        fruit_contours = cnt.copy()
        cnt_area = 0

        if cnt != []:
            fruit_contours = max(cnt, key=cv2.contourArea)
            self.contour = fruit_contours
            (x, y), r = self.detect_circle(fruit_contours)

            if r != 0:
                cnt_area =  cv2.contourArea(fruit_contours)
                fruit_circle_ratio = cnt_area / (pi * (r ** 2))
        else:
            fruit_circle_ratio = 0
        
        # elipse matching
        point1, point2, dst = self.extreme_points(fruit_contours)
        elipse_coeff = 0.4
        elipse_area = 0.5*pi*elipse_coeff*dst**2
        fruit_elipse_ratio = elipse_area/cnt_area


        # only best suited oranges passes
        if fruit_circle_ratio > 0.8:
            res = 'orange'

        # only best suited bananas passes
        elif fruit_elipse_ratio > 2.2 and fruit_circle_ratio < 0.3:
            res = 'banana'

        # only best suited bananas passes
        elif fruit_circle_ratio < 0.25:
            res = 'banana'
        else:
            # na podstawie znalezionych danych sprawdzamy jaki co owoc
            # sprawdzenie odbedzie sie na podstawie liczby pikseli o barwach okreĹ›lonych jako pomaranczowy, czerwony lub ĹĽĂłĹ‚ty
            res = self.fruit_decision(cv2.countNonZero(self.mask_orange), cv2.countNonZero(self.mask_yellow), cv2.countNonZero(self.mask_red), fruit_circle_ratio)

        if res != self.fruit_name and False:
            img_resized = cv2.cvtColor(self.img_resized.copy(), cv2.COLOR_BGR2RGB)
            cv2.drawContours(img_resized, cnt, -1, [100, 100, 100], 3)
            self.show(img_resized, 'detected ' + self.fruit_name + ' as ' + res)

        
        self.fruit_data.fruit_type = fruit_data_transfer.fruit_dict[res]


        self.fruit_data.img_masked = self.water_shed_stand_alone(cv2.cvtColor(self.img_resized.copy(), cv2.COLOR_BGR2RGB))
        #self.fruit_data.img_masked = self.mask_background1(False)


        #print(self.fruit_data.fruit_type)
        



        return self.fruit_data
'''
# Test all fruits in subfolders of DataSet
dir = 'DataSet/'
res = [] # tablica wynikowa zawierajaca napisy jaki owoc
i = 0
dir_child = [x[0] for x in os.walk(dir)]
#dir_child = [dir, dir + 'rotten_banana']
print(dir_child)

overall_good = 0
overall_all = 0
for dir_sub in dir_child[2:]:
    print(dir_sub)
    res = []
    i = 0
    if re.search('banana', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.BANANA
    elif re.search('strawberries', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.STRAWBERRY
    elif re.search('lemon', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.LEMON
    elif re.search('orange', dir_sub):
        key_fruit = fruit_data_transfer.Fruit.ORANGE
    else:
        pass

    for filename in os.listdir(dir_sub):
        #print((dir_sub + '/' + filename))
        fr = fruit_detection(dir_sub + '/' + filename, key_fruit)
        res.append(fr.detect_fruit())
        #print("i = " + str(i))
        i = i+1



    print("banana: " + str(sum(p.fruit_type == fruit_data_transfer.Fruit.BANANA for p in res)))
    print("strawberry: " + str(sum(p.fruit_type == fruit_data_transfer.Fruit.STRAWBERRY for p in res)))
    print("lemon: " + str(sum(p.fruit_type == fruit_data_transfer.Fruit.LEMON for p in res)))
    print("orange: " + str(sum(p.fruit_type == fruit_data_transfer.Fruit.ORANGE for p in res)))
    
    count_good = sum(p.fruit_type == key_fruit  for p in res)
    count_all = len(res)

    overall_good += count_good
    overall_all += count_all
    print('effectivness: ' + str(count_good/count_all*100) + '%')
    print()

print('Correctly detected: ' + str(overall_good))
print('Incorreclty detected: ' + str(overall_all - overall_good))
print('Total checked: ' + str(overall_all))
print('overall effectivness ' + str(overall_good/overall_all*100) + '%')
'''
