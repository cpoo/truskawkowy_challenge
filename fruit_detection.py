import cv2
import numpy as np
import argparse
import copy
from math import *
import os
from matplotlib import pyplot as plt


class fruit_detection:
    def __init__(self, filename):
        # w tym miejscu wykonujemy elementarne operacje, które mają podnieść wyrazistość obrazu i ułatwić dalsze przetwarzanie
        self.filename = filename
        self.img_original = cv2.imread(self.filename)
        self.img_org = cv2.imread(self.filename)
        self.height, self.width, depth = self.img_original.shape

        scale = self.height / 100

        self.height, self.width = int(self.height / scale), int(self.width / scale)

        self.img_original = cv2.resize(self.img_original, (self.width, self.height))
        self.img_resized = copy.copy(self.img_original)
        self.fruits = {'orange': 0,
                       'banana': 1,
                       'strawberry': 2
                       }
        self.img_gray_masked = {'orange': 0,
                                'banana': 1,
                                'strawberry': 2
                                }

        self.img_original = self.hisEqulColor(self.img_original)

    def hisEqulColor(self, img):
        ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)
        channels = cv2.split(ycrcb)
        len(channels)
        cv2.equalizeHist(channels[0], channels[0])
        cv2.merge(channels, ycrcb)
        cv2.cvtColor(ycrcb, cv2.COLOR_YCR_CB2BGR, img)
        return img

    def show(self, file):
        plt.imshow(file, 'gray')
        plt.show()

    def show(self, file, title):
        plt.imshow(file, 'gray')
        plt.title(title)
        plt.show()

    def showColor(self, file, title):
        plt.imshow(file)
        plt.title(title)
        plt.show()

    def get_mask_no_background(self):

        img_original = self.img_original
        img_hsv = cv2.cvtColor(img_original, cv2.COLOR_BGR2HSV)

        # ponieważ tło jest zawsze białe lub jasno szare, znajdujemy piksele, które mają taką barwę, będzie to tło
        mask_white_neg = cv2.inRange(img_hsv, (0, 0, 0), (255, 80, 255))
        mask_white_neg = cv2.bitwise_not(mask_white_neg)

        kernel = np.ones((5, 5), np.uint8)
        mask_white_neg = cv2.morphologyEx(mask_white_neg, cv2.MORPH_CLOSE, kernel) #
        return mask_white_neg

    def find_contours(self, ):

        img_original = self.img_original
        img_hsv = cv2.cvtColor(img_original, cv2.COLOR_BGR2HSV)

        mask = self.get_mask_no_background() # oddzielamy tło od owocu
        target = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)

        img_gray = cv2.cvtColor(target, cv2.COLOR_BGR2GRAY)

        contours, _ = cv2.findContours(img_gray.copy(), cv2.RETR_EXTERNAL,
                                       cv2.CHAIN_APPROX_NONE) # znajdujemy kontur

        return contours, mask

    def detect_circle(self, contour):
        (x, y), r = cv2.minEnclosingCircle(contour)
        return (int(x), int(y)), int(r)

    def fruit_decision(self, orange, yellow, red, circle_ratio):
        # w zależności od tego, którego koloru jest najwięcej, podejmujemy decyzję o typie owocu
        maxim = max(orange, yellow, red)
        if maxim == orange:
            return 'orange'
        if maxim == red:
            return 'strawberry'
        if maxim == yellow: # w przypadku żółtego badamy jak bardzo kształt przypomina koło. jeśli słabo to jest to banan, jeśli mocno, to to cytryna
            if circle_ratio < 0.65:
                return 'banana'
            else:
                return 'lemon'

    def detect_fruit(self):
        # shape detection section
        cnt, mask = self.find_contours() # znajdujemy kontur oraz maske owocu
        # ponizej badamy jak bardzo znaleziony kontur jest podobny do koła (może się przydać przy okreslaniu typu owoca)
        if cnt != []:
            fruit_contours = max(cnt, key=cv2.contourArea)
            (x, y), r = self.detect_circle(fruit_contours)

            if r != 0:
                fruit_circle_ratio = cv2.contourArea(fruit_contours) / (pi * (r ** 2))
        else:
            fruit_circle_ratio = 0

        # color detection section
        img_hsv = cv2.cvtColor(self.img_resized, cv2.COLOR_BGR2HSV)
        #znajdujemy maski dla kolorów owoców, które przetwarzamy
        mask_orange = cv2.inRange(img_hsv, (5, 170, 20), (20, 255, 255))
        mask_yellow = cv2.inRange(img_hsv, (20, 100, 20), (40, 255, 255))
        mask_red = cv2.inRange(img_hsv, (0, 100, 20), (5, 255, 255)) + cv2.inRange(img_hsv, (160, 100, 20), (180, 255, 255))
        # na podstawie znalezionych danych sprawdzamy jaki co owoc
        # sprawdzenie odbedzie sie na podstawie liczby pikseli o barwach określonych jako pomaranczowy, czerwony lub żółty
        fruit = self.fruit_decision(cv2.countNonZero(mask_orange), cv2.countNonZero(mask_yellow), cv2.countNonZero(mask_red), fruit_circle_ratio)
        return fruit, mask

dir = 'DataSet/rotten_strawberries/'
res = [] # tablica wynikowa zawierajaca napisy jaki owoc
masks = [] # tablica wynikowa zawierajaca znalezione maski

for filename in os.listdir(dir):
    fr = fruit_detection(dir + filename) # tworzymy obiekt detektora owoców
    fruit, mask = fr.detect_fruit() # znajdujemy jaki to owoc i maske dla niego
    res.append(fruit)
    masks.append(mask)

# wyswietlanie wynikow, ile jakich owocow znaleziono
print("banana: " + str(res.count("banana")))
print("strawberry: " + str(res.count("strawberry")))
print("lemon: " + str(res.count("lemon")))
print("orange: " + str(res.count("orange")))