# import the necessary packages

import numpy as np
import cv2 as cv
from enum import Enum,auto
import CracksUtil as cu

class Fruit (Enum):
    BANANA = auto()
    ORANGE = auto()
    STRAWBERRY = auto()


import OrangeDetection
import BananaDetection

print("SCRIPT INIT")

DEBUG_MODE = False

def cracks_detection(image, imageMask, fruitType, debugMode):
    DEBUG_MODE = debugMode

    rows, cols = imageMask.shape

    # Set Default result variables
    resultImage = np.zeros((rows, cols, 1), np.uint8)
    crackDetected = False

    # Run detection
    if fruitType == Fruit.ORANGE:
        crackDetected, resultImage = OrangeDetection.extractCracks(image, imageMask, DEBUG_MODE)
    elif fruitType == Fruit.BANANA:
        crackDetected, resultImage = BananaDetection.extractCracks(image, imageMask, DEBUG_MODE)
    else:
        print("Cracks for strawberries are not supported!")
        return crackDetected, image

    # Convert results
    return crackDetected, resultImage