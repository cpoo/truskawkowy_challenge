import numpy as np
import cv2 as cv 
from matplotlib import pyplot as plt
from enum import Enum,auto
import os

class Fruit (Enum):
    BANANA = auto()
    ORANGE = auto()
    STRAWBERRY = auto()

### mould_detection ###
# inputs: (img, mask, fruit_type) : img - obraz wejściowy, mask - maska na tło, fruit_type - rodzaj owoca (enum)
# outputs: (is_mouldy, image_out) : is_mouldy - flaga czy wykryto pleśń, image_out - obraz z maską w miejscu pleśni 

def mould_detection(img, mask, fruit_type):

    indices_1 = np.where(mask!=255)
    img_masked = img.copy()
    img_masked[indices_1[0], indices_1[1], :] = [255, 0, 255]

    imgHSV = cv.cvtColor(img_masked,cv.COLOR_RGB2HSV)
    #imgHSV = cv.cvtColor(img,cv.COLOR_BGR2HSV)
    #imgHSV = img_masked

    #wymiary siatki do podziału obrazu
    grid_dim = 25

    tileGridSize=(grid_dim, grid_dim, 1)
    tilesY, tilesX, tilesZ = tileGridSize

    imgHeight, imgWidth, imgPalette = imgHSV.shape

    delta = tuple(np.remainder(tileGridSize - np.remainder(imgHSV.shape,tileGridSize),tileGridSize))

    img_extended = np.pad(imgHSV,((0,delta[0]),(0,delta[1]), (0,delta[2])),'symmetric')

    tileSize = tuple(np.divide(img_extended.shape,tileGridSize).astype('int'))
    tileHeight, tileWidth, tileDepth = tileSize
    tilePixelsNum = tileHeight*tileWidth

    histInfo_H=np.ndarray(shape=(tilesY,tilesX, 360))
    histInfo_S=np.ndarray(shape=(tilesY,tilesX, 360))
    histInfo_V=np.ndarray(shape=(tilesY,tilesX, 360))

    for ny in range(tilesY):
        for nx in range(tilesX):

            #określenie obszaru komórki
            tileContent = img_extended[ny*tileHeight:(ny+1)*tileHeight,
                                        nx*tileWidth:(nx+1)*tileWidth, :]
            #wyliczenie histogramów
            hist1 = cv.calcHist([tileContent],[0],None,[360],[0,256])
            histInfo_H[ny,nx,:]=hist1[:,0]
            hist2 = cv.calcHist([tileContent],[1],None,[360],[0,256])
            histInfo_S[ny,nx,:]=hist2[:,0]
            hist3 = cv.calcHist([tileContent],[2],None,[360],[0,256])
            histInfo_V[ny,nx,:]=hist3[:,0]

    mask = np.zeros((tilesY, tilesX), dtype=int)
    rotten_elems = 0
    #tworzenie maski
    if fruit_type == Fruit.ORANGE:
        for ny in range(tilesY):
            for nx in range(tilesX):
                if sum(histInfo_S[ny, nx, 0:50]) >= 0.5*tilePixelsNum:
                    mask[ny, nx] = 1
                elif sum(histInfo_H[ny, nx, 50:150]) >= 0.6*tilePixelsNum:
                    mask[ny, nx] = 1
                elif (sum(histInfo_H[ny, nx, 0:60]) >= 0.5*tilePixelsNum) and (sum(histInfo_S[ny, nx, 50:150]) >= 0.5*tilePixelsNum):
                    mask[ny, nx] = 1
    elif fruit_type == Fruit.BANANA:
        for ny in range(tilesY):
            for nx in range(tilesX):
                if sum(histInfo_V[ny, nx, 200:360]) <= 0.3*tilePixelsNum:
                    mask[ny, nx] = 1
    elif fruit_type == Fruit.STRAWBERRY:
        for ny in range(tilesY):
            for nx in range(tilesX):
                if sum(histInfo_S[ny, nx, 0:50]) >= 0.5*tilePixelsNum:
                    mask[ny, nx] = 1
                elif (sum(histInfo_H[ny, nx, 0:60]) >= 0.5*tilePixelsNum) and (sum(histInfo_S[ny, nx, 50:150]) >= 0.5*tilePixelsNum):
                    mask[ny, nx] = 1
    else:
        raise NotImplementedError()
    if  np.count_nonzero(mask) <= 5: 
        mask = np.zeros((tilesY, tilesX), dtype=int)
    rotten_elems = np.count_nonzero(mask)
    mask_3D = np.repeat(mask, tileHeight, axis=0)
    mask_3D = np.repeat(mask_3D, tileWidth, axis=1)
    k=[-delta[0] if delta[0]!=0 else imgHSV.shape[0], -delta[1] if delta[1]!=0 else imgHSV.shape[1]]
    mask_3D = mask_3D[:k[0],:k[1], np.newaxis]
    mask_3D = np.tile(mask_3D, (1,1,3))

    indices = np.where(mask_3D==1)
    img[indices[0], indices[1], :] = [0, 255, 0]

    if rotten_elems != 0:
        return True, img
    else:
        return False, img
