# import the necessary packages
import os
import cv2 as cv
import numpy as np
from enum import Enum,auto

import CracksDetector
import CracksUtil

DEBUG_MODE = False

dir = 'DataSet/banana/'


print(enumerate(os.listdir(dir)))
for file in (os.listdir(dir)):

    if "mask" in file or file.startswith("."):
        continue

    print("Processing: " + file)

    extension = file.split('.')[1]

    objectFile = dir + file
    objectFileMask = dir + file.replace('.' + extension, '_mask.' + extension)

    image = cv.imread(objectFile)
    mask = cv.imread(objectFileMask, cv.THRESH_BINARY)

    crackDetected, resultImage = CracksDetector.cracks_detection(image, mask, CracksDetector.Fruit.BANANA, DEBUG_MODE)

    numpy_vertical_concat = np.concatenate((image, resultImage), axis=1)
    CracksUtil.displayImage('Result', numpy_vertical_concat)

cv.destroyAllWindows()
exit(0)


