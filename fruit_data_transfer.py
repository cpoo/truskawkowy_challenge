import cv2
from enum import Enum,auto



class Fruit (Enum):
    BANANA = auto()
    LEMON = auto()
    ORANGE = auto()
    STRAWBERRY = auto()
    EMPTY = auto()

fruit_dict = {'banana' : Fruit.BANANA,
              'lemon' : Fruit.LEMON,
              'orange' : Fruit.ORANGE,
              'strawberry' : Fruit.STRAWBERRY
              }



class fruit_data_transfer:
    def __init__(self, filename):
        self.filename = filename
        self.fruit_type = Fruit.EMPTY
        self.img_original = []
        self.img_masked = []
        self.img_mask = []
