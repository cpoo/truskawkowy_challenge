# import the necessary packages

from skimage import exposure
import numpy as np
import argparse
import imutils
import cv2

print("SCRIPT INIT")


def removeBackgorund(image, mask):
    return cv2.bitwise_and(image, image, mask=mask)


def normalizeImage(image):
    return cv2.normalize(image, image, 0, 255, cv2.NORM_MINMAX)


def reduceColors(image):
    img = image
    Z = img.reshape((-1, 3))
    # convert to np.float32
    Z = np.float32(Z)
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 4
    ret, label, center = cv2.kmeans(Z, K, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)
    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))

    displayImage('test reduction', res2)
    return res2
    #return cv2.pyrMeanShiftFiltering(image, 50, 50)


def displayImage(text, image):
    cv2.imshow(text, image)
    cv2.waitKey(0)


def getContourMap(originalImage, curve):
    contourMask = np.zeros(originalImage.shape[:2], np.uint8)
    cv2.fillPoly(contourMask, pts=[curve], color=(255, 255, 255))
    return contourMask

def getWhiteColorMask(image):
    lower_red = np.array([0, 0, 20])
    upper_red = np.array([255, 50, 255])
    hsv_banana = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask_white = cv2.inRange(hsv_banana, lower_red, upper_red)
    return mask_white

def isColorInRange(originalImage, contourMask):
    meanBGR = cv2.mean(originalImage, contourMask)
    print meanBGR
    bgrValue = np.uint8([[[meanBGR[2], meanBGR[1], meanBGR[0]]]])
    meanHSV = cv2.cvtColor(bgrValue, cv2.COLOR_BGR2HSV)
    print meanHSV
    h, s, v = cv2.split(meanHSV)
    if s < 35:
        return True
    else:
        return False


def getAverageObjectColorHSV(image, mask):
    meanBGR = cv2.mean(image, mask)
    bgrValue = np.uint8([[[meanBGR[2], meanBGR[1], meanBGR[0]]]])
    meanHSV = cv2.cvtColor(bgrValue, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(meanHSV)
    return h, s, v


def getObjectAreaSize(mask):
    img_edges = cv2.Canny(mask, 100, 200)
    contours, hierarchy = cv2.findContours(img_edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    maxSize = 0
    for con in contours:
        approxCurve = cv2.approxPolyDP(con, 10, True)
        area = cv2.contourArea(approxCurve)
        if area > maxSize:
            maxSize = area

    return area


def getEdges(image, minumumArea):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    contourMask = np.zeros(img_original.shape[:2], np.uint8)
    cv2.drawContours(contourMask, contours, -1, (255, 255, 255), 5)
    return contourMask


def fillEdges(image, minumumArea, maximumArea, lowColorImage, imageAverageColorHsvValue):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    contourMask = np.zeros(img_original.shape[:2], np.uint8)

    for i in range(len(contours)):
        approxCurve = cv2.approxPolyDP(contours[i], 10, True)
        area = cv2.contourArea(approxCurve)

        if area < minumumArea:
            continue

        if area > maximumArea:
            continue

        # COMPARE COLORS
        contourMaskTemp = getContourMap(image, approxCurve)
        h, s, v = getAverageObjectColorHSV(lowColorImage, contourMaskTemp)

        print 'Values to compare'
        print('IMAGE AVERAGE: ',imageAverageColorHsvValue[1], 'AREA AVERAGE: ', s)

        absValue = abs(imageAverageColorHsvValue[1] - s)

        if absValue / imageAverageColorHsvValue[1] > 0.4:
            print 'MATCHED !'
            cv2.fillPoly(contourMask, pts=[approxCurve], color=(255, 255, 255))

        # cv2.drawContours(contourMask, approxCurve, -1, (255), 1)

    return contourMask


BANANA = 'DataSet/DSC_2712.jpg'
BANANA_MASK = 'DataSet/DSC_2712_MASK.jpg'

# BANANA = 'DataSet/DSC_2713.jpg'
# BANANA_MASK = 'DataSet/DSC_2713_MASK.jpg'
#
# BANANA = 'DataSet/banana.jpg'
# BANANA_MASK = 'DataSet/banana_mask.jpg'

img_original = cv2.imread(BANANA)
img_mask = cv2.imread(BANANA_MASK, cv2.THRESH_BINARY)
img_normalized = normalizeImage(img_original);

# USUWAMY TLO ZA POMOCA MASKI
img_no_background = cv2.bitwise_and(img_normalized, img_normalized, mask=img_mask)
img_no_background = removeBackgorund(img_normalized, img_mask)
displayImage('Banan - wyciete tlo', img_no_background)

# OGRANICZAMY LICZBE KOLOROW
img_reduced = reduceColors(img_no_background)
displayImage('Banan - ograniczenie kolorow', img_reduced)


# LICZYMYS SREDNI KOLOR OBIEKTU
objectAverageColorHsv = getAverageObjectColorHSV(img_reduced, img_mask)
print 'Average Object Color:'
print('h=',objectAverageColorHsv[0],' s=',objectAverageColorHsv[1],' v=',objectAverageColorHsv[2])

# LICZYMY OBSZAR JAKI ZAJMUJE OBIEKT
objectArea = getObjectAreaSize(img_mask)
print('Area of the object = ', objectArea)

# SZUKAMY OBSZAROW
img_gray = cv2.cvtColor(img_reduced, cv2.COLOR_BGR2GRAY)
img_gray_filtered = cv2.bilateralFilter(img_gray, 11, 17, 17)
img_edges = cv2.Canny(img_gray, 30, 200)
displayImage('image edges', img_edges)

# WYKRYWAMY KRAWEDZIE #1
img_edges_mask = getEdges(img_edges, 10)
displayImage('FIRST EDGES', img_edges_mask)

# WYKRYWANIE ZAMKNIETYCH OBSZAROW I UTWORZENIE Z NICH MASKI #2
img_edges_regions_mask = fillEdges(img_edges_mask, objectArea/20, objectArea * 0.6, img_reduced, objectAverageColorHsv)
displayImage('CLOSED REGIONS', img_edges_regions_mask)


cv2.destroyAllWindows()
exit(0)
