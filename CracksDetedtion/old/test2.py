# import the necessary packages

from skimage import exposure
import numpy as np
import argparse
import imutils
import cv2


print("SCRIPT INIT")

BANANA = 'DataSet/DSC_2713.jpg'
BANANA_MASK = 'DataSet/DSC_2713_MASK.jpg'

# BANANA = 'DataSet/banana.jpg'
# BANANA_MASK = 'DataSet/banana_mask.jpg'

img_original = cv2.imread(BANANA)
img_original = cv2.normalize(img_original,  img_original, 0, 255, cv2.NORM_MINMAX)

img = cv2.imread(BANANA)
img_mask = cv2.imread(BANANA_MASK,cv2.THRESH_BINARY)


image = cv2.imread(BANANA, cv2.IMREAD_COLOR)  # uint8 image
cv2.imshow('Original Image', image)
cv2.waitKey(0)

raw_image = cv2.imread(BANANA)
img_original = cv2.normalize(raw_image,  raw_image, 0, 255, cv2.NORM_MINMAX)
cv2.imshow('Original Image', raw_image)
cv2.waitKey(0)

# USUWAMY TLO ZA POMOCA MASKI
bananaWithMask = cv2.bitwise_and(raw_image,raw_image ,mask = img_mask)
cv2.imshow('Original Banana Mask', bananaWithMask)
cv2.waitKey(0)

# ZNAJDUJEMY NA OBRAZIE OBSZARY NAJBLIZSZE BIALEJ BARWIE
# DETEKCJA ZIELONEGO
lower_red = np.array([0,0,20])
upper_red = np.array([255,50,255])
hsv_banana = cv2.cvtColor(bananaWithMask, cv2.COLOR_BGR2HSV)
mask_white = cv2.inRange(hsv_banana,lower_red,upper_red)
cv2.imshow('White filter', mask_white)
cv2.waitKey(0)

# Z wykrytych bialych obszarow wykrywamy krawedzie
# edge_detected_image = cv2.Canny(mask_white, 0, 50)
# cv2.imshow('Edge', edge_detected_image)
# cv2.waitKey(0)

# Stosujemy biala maske na oryginaly obraz
onlyCracks = cv2.bitwise_and(bananaWithMask, bananaWithMask, mask = mask_white)
cv2.imshow('Same pekniecia', onlyCracks)
cv2.waitKey(0)


image = cv2.imread(BANANA, cv2.IMREAD_GRAYSCALE)
normalized = cv2.equalizeHist(image)

normalized = cv2.bitwise_and(normalized,normalized ,mask = img_mask)
cv2.imshow('normalized only banana', normalized)
cv2.waitKey(0)


gray = cv2.bilateralFilter(normalized, 11, 17, 17)

ret,thresh1 = cv2.threshold(normalized,100,120,cv2.THRESH_BINARY)
cv2.imshow('thresh1', thresh1)
cv2.waitKey(0)

edged = cv2.Canny(gray, 30, 200)
cv2.imshow('Original Banana Mask', edged)
cv2.waitKey(0)


# wykrywamy krawedzie
contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

# on the original image.
for i in range(len(contours)):

    hull = cv2.convexHull(contours[i])
    #cv2.drawContours(raw_image, [hull], -1, (255, 0, 0), 3)
    approxCurve = cv2.approxPolyDP(contours[i], 10, True)

    area = cv2.contourArea(approxCurve)


    contourMask = np.zeros(raw_image.shape[:2], np.uint8)
    cv2.fillPoly(contourMask, pts =[approxCurve], color=(255,255,255))
    #cv2.drawContours(contourMask, approxCurve, -1, (255),1)

    if ((area > 30) ):
        # CHECK IS UNDER THE CURVE IS WHITE APPROX WHITE COLOUR

        meanBGR = cv2.mean(img_original, contourMask)
        print meanBGR

        bgrValue = np.uint8([[[meanBGR[2], meanBGR[1], meanBGR[0]]]])
        meanHSV = cv2.cvtColor(bgrValue, cv2.COLOR_BGR2HSV)
        print meanHSV

        h, s, v = cv2.split(meanHSV)

        if 1 < 35:
            #cv2.imshow('Maska z kontura', contourMask)
            #cv2.waitKey(0)
            cv2.drawContours(raw_image, [approxCurve], -1, (255, 0, 0), 3)



cv2.imshow('ConvexHull', raw_image)
cv2.waitKey(0)
