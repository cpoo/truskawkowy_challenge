# import the necessary packages

from skimage import exposure
import numpy as np
import argparse
import imutils
import cv2

print("SCRIPT INIT")


def hisEqulColor(img):
    ycrcb = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)
    channels = cv2.split(ycrcb)
    len(channels)
    cv2.equalizeHist(channels[0], channels[0])
    cv2.merge(channels, ycrcb)
    cv2.cvtColor(ycrcb, cv2.COLOR_YCR_CB2BGR, img)
    return img


def removeBackgorund(image, mask):
    return cv2.bitwise_and(image, image, mask=mask)


def normalizeImage(image):
    image = hisEqulColor(image)
    image = cv2.normalize(image, image, 0, 255, cv2.NORM_MINMAX)
    return image

def displayImage(text, image):
    cv2.imshow(text, image)
    cv2.waitKey(0)

def getContourMap(originalImage, curve):
    contourMask = np.zeros(originalImage.shape[:2], np.uint8)
    cv2.fillPoly(contourMask, pts=[curve], color=(255, 255, 255))
    return contourMask

def getWhiteColorMask(image):
    lower_red = np.array([0, 0, 100])
    upper_red = np.array([180, 50, 255])
    hsv_banana = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask_white = cv2.inRange(hsv_banana, lower_red, upper_red)

    res, tresh = cv2.threshold(mask_white, 0, 50, cv2.THRESH_BINARY)

    numpy_horizontal = np.hstack((mask_white, tresh))

    #displayImage('WHITE MASK', numpy_horizontal)

    return tresh

def getAverageObjectColorHSV(image, mask):
    b,g,r,t = cv2.mean(image, mask)
    bgrAverage = np.uint8([[[b, g, r]]])
    meanHSV = cv2.cvtColor(bgrAverage, cv2.COLOR_BGR2HSV)
    h,s,v = meanHSV[0][0]
    return(h,s,v)

def getObjectAreaSize(mask):
    img_edges = cv2.Canny(mask, 0, 200)
    contours, hierarchy = cv2.findContours(img_edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    maxSize = 0
    for con in contours:
        approxCurve = cv2.approxPolyDP(con, 10, True)
        area = cv2.contourArea(approxCurve)
        if area > maxSize:
            maxSize = area

    return area


def getEdges(image, minumumArea):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    contourMask = np.zeros(img_original.shape[:2], np.uint8)

    cv2.drawContours(contourMask, contours, -1, (255, 255, 255), 5)

    return contourMask

def markDetectedCracks(image, cracks_mask):
    contours, hierarchy = cv2.findContours(cracks_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cv2.drawContours(image, contours, -1, (0, 255, 0), 3)
    return image



def fillEdges(image_original, image, minumumArea, maximumArea, lowColorImage, imageAverageColorHsvValue):
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    contourMask = np.zeros(img_original.shape[:2], np.uint8)

    for i in range(len(contours)):
        approxCurve = cv2.approxPolyDP(contours[i], 10, True)
        area = cv2.contourArea(approxCurve)

        if area < minumumArea:
            continue

        if area > maximumArea:
            continue

        # COMPARE COLORS
        contourMaskTemp = getContourMap(image, approxCurve)
        h, s, v = getAverageObjectColorHSV(lowColorImage, contourMaskTemp)

        print('================')
        print('Background HSV = ',imageAverageColorHsvValue[0],imageAverageColorHsvValue[1],imageAverageColorHsvValue[2])
        print('OBJECT     HSV = ',h,s,v)

        if(v > 200):
            print 'CRACK MATCH!'
            cv2.fillPoly(contourMask, pts=[approxCurve], color=(255, 255, 255))
            cv2.fillPoly(image_original, pts=[approxCurve], color=(0, 255, 0))
            displayImage('contourMask', contourMask)
            cv2.waitKey()



        # cv2.drawContours(contourMask, approxCurve, -1, (255), 1)

    return image_original, contourMask

def find_contours(img_original, mask):

    img_original = img_original

    img_original = cv2.stylization(img_original, sigma_s=60, sigma_r=0.9)
    #displayImage('img_original', img_original)

    img_hsv = cv2.cvtColor(img_original, cv2.COLOR_BGR2HSV)
    target = cv2.bitwise_and(img_hsv, img_hsv, mask=mask)
    img_gray_original = cv2.cvtColor(target, cv2.COLOR_BGR2GRAY)

    img_gray_filtered = cv2.bilateralFilter(img_gray_original, 20, 20, 50)
    img_gray = img_gray_filtered - img_gray_original
    #displayImage('img_gray', img_gray)

    whiteColorMask = getWhiteColorMask(img_original)
    onlyWhiteColor = cv2.bitwise_and(img_original, img_original, mask=whiteColorMask)
    #displayImage('whiteColors', onlyWhiteColor)

    img_edges = cv2.Canny(onlyWhiteColor, 20, 200,5)
    #displayImage('EDGES', img_edges)


    contours, _ = cv2.findContours(img_edges.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)

    contourMask = np.zeros(img_original.shape[:2], np.uint8)
    cv2.drawContours(contourMask, contours, -1, (255, 255, 255), 3)
    #displayImage('contourMask', contourMask)

    contours, _ = cv2.findContours(contourMask.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    contourMask = np.zeros(img_original.shape[:2], np.uint8)

    for cnt in contours:
        approxCurve = cv2.approxPolyDP(cnt, 1, False)
        cv2.drawContours(contourMask, approxCurve, -1, (255, 255, 255), 3)
        cv2.fillPoly(contourMask, pts=[approxCurve], color=(255, 255, 255))

    return contourMask


BANANA = 'DataSet/DSC_2712.jpg'
BANANA_MASK = 'DataSet/DSC_2712_MASK.jpg'

# BANANA = 'DataSet/DSC_2713.jpg'
# BANANA_MASK = 'DataSet/DSC_2713_MASK.jpg'
#
# BANANA = 'DataSet/banana.jpg'
# BANANA_MASK = 'DataSet/banana_mask.jpg'

img_original = cv2.imread(BANANA)
img_mask = cv2.imread(BANANA_MASK, cv2.THRESH_BINARY)

# USUWAMY TLO ZA POMOCA MASKI
img_no_background = removeBackgorund(img_original, img_mask)
displayImage('Banan - wyciete tlo', img_no_background)

# WYROWNANIE  HISTOGRAMU
img_normalized = normalizeImage(img_no_background)
#displayImage('Banan - wyrownanie histogramu', img_normalized)

# LICZYMYS SREDNI KOLOR OBIEKTU
h,s,v = getAverageObjectColorHSV(img_normalized, img_mask)
print 'Average Object Color:'
print('h=',h,' s=',s,' v=',v)

# LICZYMY OBSZAR JAKI ZAJMUJE OBIEKT
objectArea = getObjectAreaSize(img_mask)
print('Area of the object = ', objectArea)

# WYKRYWAMY KRAWEDZIE #0.1
img_edges_mask = find_contours(img_normalized, img_mask)
#displayImage('image edges', img_edges_mask)

# WYKRYWANIE ZAMKNIETYCH OBSZAROW I UTWORZENIE Z NICH MASKI #2
img_final, img_edges_regions_mask = fillEdges(img_original, img_edges_mask, objectArea/100, objectArea * 0.8, img_normalized, [h,s,v])
#displayImage('CLOSED REGIONS', img_edges_regions_mask)

# NANIESIENIE PEKNIEC NA WYJSCIOWY OBRAZ
displayImage('FINAL CRACKS', img_final)

cv2.destroyAllWindows()
exit(0)
