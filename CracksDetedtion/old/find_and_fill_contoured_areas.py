# import the necessary packages

from skimage import exposure
import numpy as np
import argparse
import imutils
import cv2


print("SCRIPT INIT")

def displayImage(text, image):
    cv2.imshow(text, image)
    cv2.waitKey(0)

def fillEnclosedAreas(img):
    displayImage('Result', img)


TEST_MASK = 'DataSet/testMap.png'
img_original = cv2.imread(TEST_MASK)

# SZUKAMY OBSZAROW
img_gray = cv2.cvtColor(img_original, cv2.COLOR_BGR2GRAY)
img_gray_filtered = cv2.bilateralFilter(img_gray, 11, 17, 17)
img_edges = cv2.Canny(img_gray_filtered, 30, 80)
displayImage('image edges', img_edges)

# wykrywamy krawedzie
contours, hierarchy = cv2.findContours(img_gray_filtered, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
# on the original image.
for i in range(len(contours)):

    hull = cv2.convexHull(contours[i])
    #cv2.drawContours(raw_image, [hull], -1, (255, 0, 0), 3)
    approxCurve = cv2.approxPolyDP(contours[i], 10, True)
    area = cv2.contourArea(approxCurve)

    contourMask = np.zeros(img_original.shape[:2], np.uint8)
    cv2.fillPoly(contourMask, pts =[approxCurve], color=(255,255,255))
    cv2.drawContours(contourMask, approxCurve, -1, (255),1)


displayImage('FILLLED  BODY', img_original)