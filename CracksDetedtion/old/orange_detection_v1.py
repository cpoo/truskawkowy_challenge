# import the necessary packages

import numpy as np
import cv2 as cv
import os

print("SCRIPT INIT")

DEBUG_MODE = True

def removeBackgorund(image, mask):
    return cv.bitwise_and(image, image, mask=mask)

def displayImage(text, image):
    cv.imshow(text, image)
    cv.waitKey(0)

def debugImage(text, image):
    if DEBUG_MODE:
        cv.imshow(text, image)
        cv.waitKey(0)

def getObjectAreaSize(img_edges):
    contours, hierarchy = cv.findContours(mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    maxSize = 0
    for con in contours:
        approxCurve = cv.approxPolyDP(con, 10, True)
        area = cv.contourArea(approxCurve)
        if area > maxSize:
            maxSize = area

    print("Object area: ", maxSize)
    return maxSize

def getAverageObjectColorHSV(image, mask):
    b, g, r, t = cv.mean(image, mask)
    bgrAverage = np.uint8([[[b, g, r]]])
    meanHSV = cv.cvtColor(bgrAverage, cv.COLOR_BGR2HSV)
    h, s, v = meanHSV[0][0]
    return (h, s, v)

def hisEqulColor(img):
    ycrcb = cv.cvtColor(img, cv.COLOR_BGR2YCR_CB)
    channels = cv.split(ycrcb)
    len(channels)
    cv.equalizeHist(channels[0], channels[0])
    cv.merge(channels, ycrcb)
    cv.cvtColor(ycrcb, cv.COLOR_YCR_CB2BGR, img)
    return img

def isCrack(image, color, contour):
    # Contour position and size
    x, y, w, h = cv.boundingRect(contour)

    # Draw Crack
    rows, cols, channels = image.shape
    fullMask = np.zeros((rows, cols, 1), np.uint8)
    cv.fillPoly(fullMask, pts=[contour], color=(255))

    crack = image[y:y + h, x:x + w]
    crackMask = fullMask[y:y + h, x:x + w]

    # Get Crack Only
    crackNoBackground = removeBackgorund(crack, crackMask)
    debugImage('',crackNoBackground)

    # Prepare crack mean color to compare
    crackColors = getAverageObjectColorHSV(crack, crackMask)
    a1Val = int(crackColors[0])
    a2Val = int(color[0])
    b1Val = int(crackColors[1])
    b2Val = int(color[1])
    b3Val = int(crackColors[2])
    b3Val = int(color[2])

    # Check shape
    rect = cv.minAreaRect(contour)
    box = cv.boxPoints(rect)
    box = np.int0(box)
    box_width = rect[1][0]
    box_height = rect[1][1]

    sizeRato = 1
    if(box_width > box_height):
        sizeRato = box_width / box_height
    else:
        sizeRato = box_height / box_width

    print(sizeRato)

    # Check color difference
    if abs(a1Val - a2Val) <= 4:
        print('Wrong Color')
        return False
    if sizeRato <= 2:
        print('Wrong Shape')
        return False

    return True

def extractCracks(image, mask):
    # Prepare object size
    objectArea = getObjectAreaSize(mask)

    image = removeBackgorund(image, mask)
    image = hisEqulColor(image.copy())
    image_con = image.copy()

    # Prepare mean image value
    imageAverageColor = getAverageObjectColorHSV(image, mask)
    print('OBJECT COLOR MEAN', imageAverageColor)

    debugImage('', image)

    imageHSV = cv.cvtColor(image, cv.COLOR_RGB2HSV)
    channels = cv.split(imageHSV)
    image_gray = channels[2]

    image_gray = channels[1]
    thresh = cv.adaptiveThreshold(image_gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 261, 5)

    debugImage('', thresh)

    kernel = np.ones((4,4), np.uint8)
    closing = cv.morphologyEx(thresh, cv.MORPH_CLOSE, kernel)
    debugImage('', closing)

    closing = removeBackgorund(closing, mask)
    contours, test = cv.findContours(closing, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    # Prepare new clar mask for all contours
    rows, cols = closing.shape
    contoursMap = np.zeros((rows, cols, 1), np.uint8)
    cv.drawContours(contoursMap, contours, -1, (255, 255, 255), 3)
    debugImage('', contoursMap)

    contours = cv.findContours(closing, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)[0]

    for con in contours:
        approxCurve = cv.approxPolyDP(con, 10, True)
        if cv.contourArea(approxCurve) > objectArea * 0.1:
            continue

        if cv.contourArea(approxCurve) < objectArea / 100:
            continue

        if isCrack(image, imageAverageColor, con):
            cv.fillPoly(image_con, pts=[con], color=(0, 0, 255))
            print('Contour matched!')

    debugImage('', image_con)

dir = '../DataSet/orange/'

print(enumerate(os.listdir(dir)))

for file in (os.listdir(dir)):

    if "mask" in file or file.startswith("."):
        continue

    print("Processing: " + file)

    extension = file.split('.')[1]

    objectFile = dir + file
    objectFileMask = dir + file.replace('.' + extension, '_mask.' + extension)

    image = cv.imread(objectFile)
    mask = cv.imread(objectFileMask, cv.THRESH_BINARY)

    extractCracks(image, mask)

cv.destroyAllWindows()
exit(0)


