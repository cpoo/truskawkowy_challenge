# -*- coding: utf-8 -*-
import numpy as np
import cv2 as cv
from enum import Enum
from matplotlib import pyplot as plt
import os

class Fruit (Enum):
    BANANA = 0
    ORANGE = 1
    STRAWBERRY = 2

def hisEqulColor(img):
    ycrcb = cv.cvtColor(img, cv.COLOR_BGR2YCR_CB)
    channels = cv.split(ycrcb)
    len(channels)
    cv.equalizeHist(channels[0], channels[0])
    cv.merge(channels, ycrcb)
    cv.cvtColor(ycrcb, cv.COLOR_YCR_CB2BGR, img)
    return img

def mould_detection(input_filename, output_filename, fruit_type):

    img = cv.imread(input_filename)
    img = hisEqulColor(img)
    imgHSV = cv.cvtColor(img,cv.COLOR_BGR2HSV)

    #wymiary siatki do podziału obrazu
    grid_dim = 120

    tileGridSize=(grid_dim, grid_dim, 1)
    tilesY, tilesX, tilesZ = tileGridSize

    imgHeight, imgWidth, imgPalette = imgHSV.shape

    delta = tuple(np.remainder(tileGridSize - np.remainder(imgHSV.shape,tileGridSize),tileGridSize))

    img_extended = np.pad(imgHSV,((0,delta[0]),(0,delta[1]), (0,delta[2])),'symmetric')

    tileSize = tuple(np.divide(img_extended.shape,tileGridSize).astype('int'))
    tileHeight, tileWidth, tileDepth = tileSize
    tilePixelsNum = tileHeight*tileWidth

    histInfo_H=np.ndarray(shape=(tilesY,tilesX, 360))
    histInfo_S=np.ndarray(shape=(tilesY,tilesX, 360))
    histInfo_V=np.ndarray(shape=(tilesY,tilesX, 360))

    for ny in range(tilesY):
        for nx in range(tilesX):

            #określenie obszaru komórki
            tileContent = img_extended[ny*tileHeight:(ny+1)*tileHeight,
                                        nx*tileWidth:(nx+1)*tileWidth, :]
            #wyliczenie histogramów
            hist1 = cv.calcHist([tileContent],[0],None,[360],[0,256])
            histInfo_H[ny,nx,:]=hist1[:,0]
            hist2 = cv.calcHist([tileContent],[1],None,[360],[0,256])
            histInfo_S[ny,nx,:]=hist2[:,0]
            hist3 = cv.calcHist([tileContent],[2],None,[360],[0,256])

    mask = np.zeros((tilesY, tilesX), dtype=int)
    rotten_elems = 0
    #tworzenie maski
    if fruit_type == Fruit.ORANGE:
        for ny in range(tilesY):
            for nx in range(tilesX):
                if sum(histInfo_S[ny, nx, 40:250]) >= 0.7*tilePixelsNum and sum(histInfo_H[ny, nx, 20:70]) >= 0.7*tilePixelsNum:
                    mask[ny, nx] = 1
    elif fruit_type == Fruit.BANANA:
        for ny in range(tilesY):
            for nx in range(tilesX):
                if sum(histInfo_V[ny, nx, 200:360]) <= 0.7*tilePixelsNum:
                    mask[ny, nx] = 1
    elif fruit_type == Fruit.STRAWBERRY:
        for ny in range(tilesY):
            for nx in range(tilesX):
                if sum(histInfo_S[ny, nx, 0:170]) >= 0.9*tilePixelsNum and sum(histInfo_H[ny, nx, 180:280]) >= 0.8*tilePixelsNum:
                    mask[ny, nx] = 1
    else:
        raise NotImplementedError()
    if  np.count_nonzero(mask) <= 5:
        mask = np.zeros((tilesY, tilesX), dtype=int)
    rotten_elems = np.count_nonzero(mask)
    mask_3D = np.repeat(mask, tileHeight, axis=0)
    mask_3D = np.repeat(mask_3D, tileWidth, axis=1)
    k=[-delta[0] if delta[0]!=0 else imgHSV.shape[0], -delta[1] if delta[1]!=0 else imgHSV.shape[1]]
    mask_3D = mask_3D[:k[0],:k[1], np.newaxis]
    mask_3D = np.tile(mask_3D, (1,1,3))

    indices = np.where(mask_3D==1)
    img[indices[0], indices[1], :] = [0, 255, 0]

    if not cv.imwrite(output_filename, img):
        raise Exception("Could not write image")
    else:
        print('IMAGE SAVED IN: ' + output_filename)


    if rotten_elems != 0:
        return True
    else:
        return False


# dir = 'DataSet/orange/'
# for i,filename in enumerate(os.listdir(dir)):
#     print('Loading: ' + filename)
#     result = mould_detection(dir + filename, 'DataSet/output_oranges/' + filename, Fruit.ORANGE)
#     print('Result: ', result)

dir = '../DataSet/strawberry/'
for i,filename in enumerate(os.listdir(dir)):
    print('Loading: ' + filename)
    result = mould_detection(dir + filename, 'DataSet/output_strawberry/' + filename, Fruit.STRAWBERRY)
    print('Result: ', result)

exit(0)