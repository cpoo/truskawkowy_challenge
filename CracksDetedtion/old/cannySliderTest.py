# script for tuning parameters
import cv2
import numpy as np
import argparse

# parse argument

RED = 'DataSet/red.png'
RED_MASK = 'DataSet/red_mask.png'

img = cv2.imread(RED)
img_mask = cv2.imread(RED_MASK, cv2.THRESH_BINARY)

meanBGR = cv2.mean(img, img_mask)
print meanBGR

b,g,r,t = cv2.mean(img, img_mask)
print(b,g,r)
bgrAverage = np.uint8([[[b, g, r]]])
meanHSV = cv2.cvtColor(bgrAverage, cv2.COLOR_BGR2HSV)
h,s,v = meanHSV[0][0]
print(h,s,v)
