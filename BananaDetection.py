# import the necessary packages

import numpy as np
import cv2 as cv
import CracksUtil as cu

DEBUG_MODE = False

def isCrack(image, color, contour):
    # Contour position and size
    approxCurve = cv.approxPolyDP(contour, 5, True)

    x, y, w, h = cv.boundingRect(approxCurve)

    # Draw Crack on empty mask
    rows, cols, channels = image.shape
    fullMask = np.zeros((rows, cols, 1), np.uint8)
    cv.fillPoly(fullMask, pts=[approxCurve], color=(255))

    # Cut crack mask from the full size mask
    crack = image[y:y + h, x:x + w]
    crackMask = fullMask[y:y + h, x:x + w]

    # Cut crack (without background)
    crackNoBackground = cu.removeBackgorund(crack, crackMask)
    cu.debugImage('',crackNoBackground)

    # Prepare crack mean color to compare
    crackColors = cu.getAverageObjectColorHSV(crack, crackMask)
    a1Val = int(crackColors[0])
    a2Val = int(color[0])
    b1Val = int(crackColors[1])
    b2Val = int(color[1])
    b3Val = int(crackColors[2])
    b3Val = int(color[2])

    # Check if object shape is more rectangular than square
    rect = cv.minAreaRect(contour)
    box = cv.boxPoints(rect)
    box_width = rect[1][0]
    box_height = rect[1][1]

    sizeRato = 1
    if(box_width > box_height):
        sizeRato = box_width / box_height
    else:
        sizeRato = box_height / box_width

    print(sizeRato)

    # Check color difference
    if abs(a1Val - a2Val) < 2:
        print('Wrong Color')
        return False
    if sizeRato < 4:
        print('Wrong Shape')
        return False

    return True

def extractCracks(image, mask, debugMode):
    image_original = image.copy()
    DEBUG_MODE = debugMode
    cu.DEBUG_MODE = DEBUG_MODE

    # Prepare object size
    objectArea = cu.getObjectAreaSize(mask)

    image = cu.removeBackgorund(image, mask)
    image = cu.hisEqulColor(image.copy())
    image_con = image.copy()

    # Prepare mean image value
    imageAverageColor = cu.getAverageObjectColorHSV(image, mask)
    print('OBJECT COLOR MEAN', imageAverageColor)

    cu.debugImage('', image)

    imageHSV = cv.cvtColor(image, cv.COLOR_RGB2HSV)
    channels = cv.split(imageHSV)
    image_gray = channels[2]

    thresh = cv.adaptiveThreshold(image_gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY, 231, 7)
    cu.debugImage('', image_gray)
    cu.debugImage('', thresh)

    kernel = np.ones((4,4), np.uint8)
    closing = cv.morphologyEx(thresh, cv.MORPH_ERODE, kernel)

    cu.debugImage('', closing)

    closing = cu.removeBackgorund(closing, mask)

    contours, test = cv.findContours(closing, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    # Prepare new clar mask for all contours
    rows, cols = closing.shape
    contoursMap = np.zeros((rows, cols, 1), np.uint8)
    cv.drawContours(contoursMap, contours, -1, (255, 255, 255), 3)
    cu.debugImage('', contoursMap)

    contours = cv.findContours(closing, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)[0]

    rows, cols, layers = image_con.shape
    resultMask = np.zeros((rows, cols, 1), np.uint8)

    crackDetected = False
    for con in contours:
        approxCurve = cv.approxPolyDP(con, 10, True)
        if cv.contourArea(approxCurve) > objectArea * 0.3:
            continue

        if cv.contourArea(approxCurve) < objectArea / 100:
            continue

        if isCrack(image, imageAverageColor, con):
            cv.fillPoly(image_original, pts=[con], color=(0, 0, 255))
            cv.fillPoly(image_con, pts=[con], color=(0, 0, 255))
            cv.fillPoly(resultMask, pts=[con], color=(255, 255, 255))
            crackDetected = True
            print('Contour matched!')

    return crackDetected, image_original
