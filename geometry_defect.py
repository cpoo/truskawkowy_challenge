import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from enum import Enum,auto

import fruit_detection_m

############################################################################## 

class Fruit (Enum):
    ORANGE = auto()
    STRAWBERRY = auto()

############################################################################## 
     
def imgshow(img, title, gray, show):   
    
    if(show == 1):
        
        if(gray == 1):
            plt.imshow(img, 'gray', interpolation = 'bicubic')
            
        else:
            plt.imshow(img, interpolation = 'bicubic')
            
        plt.axis('off')
        plt.title(title)
        plt.show()
                 
############################################################################## 

def find_contours(img_original, fruit_type):

    if(fruit_type == Fruit.ORANGE):
        param1 = (0,0,0)
        param2 = (40,255,255)      
    elif(fruit_type == Fruit.STRAWBERRY):
        param1 = (0,0,0)
        param2 = (10,255,255)   

    img_hsv = cv.cvtColor(img_original, cv.COLOR_RGB2HSV)

    mask = cv.inRange(img_hsv, param1, param2)
    mask_white_neg = cv.inRange(img_hsv, (0,0,0), (255,80,255) ) 
    
    mask_white_neg = cv.bitwise_not(mask_white_neg)

    mask = cv.bitwise_and(mask_white_neg, mask)
    target = cv.bitwise_and(img_hsv, img_hsv, mask=mask)  

    return mask

############################################################################## 

def find_geometry_defect(img_original, img_mask, fruit_type):    
  
#################
    show = 0    # flaga do wyswietlania obrazow krok po kroku
    fill = 0    # flaga do zaznaczania i pokazania uszkodzenia
#################
     
    if(fruit_type == Fruit.ORANGE):
        param = (17,0,125)      
        param2 = (255,255,255)
        param_area = 1300   #100
    elif(fruit_type == Fruit.STRAWBERRY):
        param = (6,0,0)
        param2 = (255,255,255)
        param_area = 500
    
    imgshow(img_original, 'Original image', 0, show)
    imgshow(img_mask, 'Original image mask', 1, show)
    
    img_target = np.copy(img_original)
    img_target = cv.cvtColor(img_target, cv.COLOR_RGB2HSV)
    img_target = cv.bitwise_and(img_target, img_target, mask = img_mask)
    imgshow(img_target, 'Target image HSV', 0, show)
       
    imgshow(img_target[:,:,0], 'Channel H', 1, show)
    imgshow(img_target[:,:,1], 'Channel S', 1, show)
    imgshow(img_target[:,:,2], 'Channel V', 1, show)
    
#########
    if(fruit_type == Fruit.ORANGE):

        op_ch = np.copy(img_target[:,:,0])
    
        mean_br = cv.mean(op_ch, mask=img_mask)
        print("#########  MEAN VALUE:  " + str(mean_br[0]))
                                              
        if(mean_br[0] > 10):
            sub = int(mean_br[0] - 9)      #8,9 git
            value = np.ones(op_ch.shape, dtype = int) * sub
            op_ch = op_ch - value
    
        mean_br = cv.mean(op_ch, mask=img_mask)
        print("#########  MEAN VALUE MOD:  " + str(mean_br[0]))
        
        op_ch = cv.bitwise_and(op_ch, op_ch, mask = img_mask)
        imgshow(op_ch, 'Channel H MODED', 1, show)
        
        img_target[:,:,0] = op_ch
#########   
    
    defect_mask = cv.inRange(img_target, param, param2)
    imgshow(defect_mask, 'Defect mask', 1, show)
    
    contours, _ = cv.findContours(defect_mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    
    isGOOD = 1
    defect_mask = np.zeros(defect_mask.shape) + 255
    img_fill = np.copy(img_original)
    
    for cnt in contours:
        
        area = cv.contourArea(cnt)
        if area < param_area or area > 2500000:       
            continue
        if len(cnt) < 5:
            continue
        
        if(fill == 1):
            ellipse = cv.fitEllipse(cnt)
            cv.ellipse(img_fill, ellipse, (200,0,0) , 10)        
            cv.fillPoly(img_fill, [cnt], color=(80,0,0))
     
        cv.fillPoly(defect_mask, [cnt], color=(0,0,0))
     
        isGOOD = 0
        
    defect_mask = cv.inRange(defect_mask, 255, 255)
            
    if(isGOOD == 1):
        imgshow(img_original, 'GOOD', 0, show)
        
        return False, img_original
    else:
        imgshow(img_fill, 'BAD', 0, show)
        
        img_defect = cv.bitwise_and(img_original, img_original, mask = defect_mask)
        
        return True, img_defect
        
############################   TEST   ######################################## 

# fr = fruit_detection_m.fruit_detection('DataSet/fresh_oranges/1.jpg', Fruit.ORANGE)
# img_original = fr.detect_fruit().img_original
# img_mask = fr.detect_fruit().img_mask

# img_original = cv.imread('O/34.png')
# img_original = cv.cvtColor(img_original, cv.COLOR_BGR2RGB)
# img_mask = find_contours(img_original, Fruit.ORANGE)

# is_g, defect = find_geometry_defect(img_original, img_mask, Fruit.ORANGE)

# imgshow(defect, 'Defect', 1, 1)

##########
   
# fr = fruit_detection_m.fruit_detection('DataSet/Strawberry/57.jpg', Fruit.STRAWBERRY)
# img_original = fr.detect_fruit().img_original
# img_mask = fr.detect_fruit().img_mask

# img_original = cv.imread('S/57.jpg')
# img_original = cv.cvtColor(img_original, cv.COLOR_BGR2RGB)
# img_mask = find_contours(img_original, Fruit.STRAWBERRY)

# is_g, defect = find_geometry_defect(img_original, img_mask, Fruit.STRAWBERRY)

# imgshow(defect, 'Defect', 1, 1)
