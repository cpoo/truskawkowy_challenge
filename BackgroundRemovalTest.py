import cv2 as cv
import numpy as np
import os
import matplotlib.pyplot as plt


def waterShade(image):
    img = image

    # podbicie wibrancji
    image = cv.cvtColor(image, cv.COLOR_RGB2HSV)
    image[:, :, 1] = (image[:, :, 1] * 2.8).clip(0, 255)
    image = cv.cvtColor(image, cv.COLOR_HSV2RGB)

    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    thresh = cv.adaptiveThreshold(gray, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV, 9, 3)
    #plt.imshow(thresh,'gray')
    #plt.show()

    kernel = np.ones((3, 3), np.uint8)
    thresh = cv.dilate(thresh, kernel, iterations=1)

    #wypełnienie dziur w masce
    im_floodfill = thresh.copy()
    h, w = thresh.shape[:2]
    mask2 = np.zeros((h + 2, w + 2), np.uint8)
    cv.floodFill(im_floodfill, mask2, (0, 0), 255);
    im_floodfill_inv = cv.bitwise_not(im_floodfill)
    thresh = thresh | im_floodfill_inv
    #plt.imshow(thresh,'gray')
    #plt.show()

    opening = cv.morphologyEx(thresh, cv.MORPH_OPEN, kernel, iterations=5)
    #plt.imshow(opening,'gray')
    #plt.show()

    #sure background
    sure_bg = cv.dilate(thresh, kernel, iterations=3)
    #plt.imshow(sure_bg,'gray')
    #plt.show()

    dist_transform = cv.distanceTransform(opening, cv.DIST_L2, 5)
    #plt.imshow(dist_transform)
    #plt.show()

    #sure foreground
    ret, sure_fg = cv.threshold(dist_transform, 0.2 * dist_transform.max(), 255, 0)
    sure_fg = np.uint8(sure_fg)
    plt.imshow(sure_fg,'gray')
    plt.show()

    unknown = cv.subtract(sure_bg, sure_fg)
    ret, markers = cv.connectedComponents(sure_fg)
    markers = markers + 1
    markers[unknown == 255] = 0
    markers = cv.watershed(image, markers)
    img[markers == -1] = [0, 0, 0]

    markers = cv.convertScaleAbs(markers)
    ret, thresh = cv.threshold(markers,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    res = cv.bitwise_and(img,img,mask = thresh)

    plt.imshow(res)
    plt.show()



dir_sub = 'DataSet/rotten_oranges'

for filename in os.listdir(dir_sub):
    image = cv.imread(dir_sub + '/' + filename)
    image = cv.cvtColor(image,cv.COLOR_BGR2RGB)
    waterShade(image)