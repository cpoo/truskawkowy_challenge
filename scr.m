%im = imread('DataSet\rotten_strawberries\IMG_20200317_093328.jpg')

% Initialization steps.
clc;    % Clear the command window.
close all;  % Close all figures (except those of imtool.)
clear;  % Erase all existing variables. Or clearvars if you want.
workspace;  % Make sure the workspace panel is showing.
format long g;
format compact;
fontSize = 20;
A=imread('DataSet\rotten_strawberries\IMG_20200317_093328.jpg');
grayImage = double(rgb2gray(A));
subplot(2,2,1);
imshow(grayImage, [])
axis on;
Y = fftshift(fft2(grayImage));
subplot(2,2,2);
imshow(log(Y), []);
axis on;
[rows, columns] = size(Y)
B= Y;
centerX = round(columns / 2)
centerY = round(rows / 2)
% Filter: Erase center spike.
filterWidth = 5;
B(centerY-filterWidth:centerY+filterWidth, centerX-filterWidth : centerX+filterWidth) = 0;
subplot(2,2,3);
imshow(log(B), [])
axis on;
% Convert back to spatial domain.
filteredImage = abs(ifft2(ifftshift(B)));
max(filteredImage(:))
min(filteredImage(:))
subplot(2,2,4);
imshow(filteredImage, [])
axis on;